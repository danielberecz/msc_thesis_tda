## Homology

I will now describe some homological concepts that (like _homology groups_ and
_Betti numbers_), that are related group theoretic definitions, (like cycle and
boundary) which are needed for the following definitions. A more detailed
description can be found in [@zomorodian2005topology].

**Definition (homology group)** The $k$th homology group of complex is

$$
H_k = Z_k / B_k = \text{ker} \delta_k / \text{im} \delta_{k+1}
$$
where $Z_k$ is the $k$th cycle group, $Z_k$ is the $k$th boundary group, 
$\text{ker} \delta_k$ is the kernel of chain $\delta_k$, and
$\text{im}\delta_{k+1}$ is the image of $\delta_{k+1}$ in $\delta_{k}$.  
To make these concepts more concrete, lets take a triangle, $\delta_2[a,b,c]$.
Its boundaries are all one dimensional subsimplices (i.e. edges):

$$
\delta_2[a,b,c] = [b,c] - [a,c] + [a,b]
$$

these simplices also form a cycle (from any point we can travel along side the
triangle borders back to that point). These edges are the _images_
(representations) in the 2-simplex, from 1-dimensional simplexes.  
The definition for kernels are the following:

**Definition (kernel)** Let $\phi : G \rightarrow G'$ a morphism. The kernel of
$\phi$ is the subgroup $\phi^{-1}(\{e'\}) \subseteq G$ which consists of all
elements of $G$ which are mapped by $\phi$ to the identity $e'$ of $G$
[@zomorodian2005topology].

**Definition (kth Betti number)** The kth Betti number, denoted $\Beta_k (H_k)$
of a simplical complex, $K$ is the rank of the free part of $H_k$.

The $k$th Betti number shows how many $k$-dimensional "holes" are on a
topological surface:

* $b_0$ is the number of connected components,
* $b_1$ is the number of 1-dimensional holes,
* $b_2$ is the number of 2-dimensional holes, called void.

The number of various dimensional Betti numbers of surfaces classifies them into
equivalency classes, meaning if two topological surfaces have the same number 
of $k$-dimensional features (holes, voids, etc.), then they can be considered 
equal up to equivalency. The persistence algorithms described below calculate
these for the data sets to look for an underlying structure.

In the following chapter, I present here some of the abstract algebraic theory
needed to understand homotopical and homological equivalence. These definitions
follow [@zomorodian2005topology]. For the needed group theoretic definitions
consult [@dummit2004abstract].

### Simplical Complexes

In the following I will describe the geometric definition of simplices.

**Definition (combinations)** Let
$S=\{p_0,p_1,\ldots,p_k\}\subseteq\mathbb{R}^d$ a set of points. We say 
$x=\sum_{i=0}^k \lambda_i p_i$, for some $\lambda_i \in \mathbb{R}$ is a linear
combinations of this points. The combination is convex if $\lambda_i \geq 0$,
for all $i$. a linear combination is _affine_ if $\sum_{i=0}^k \lambda_i p_i=1$.
The set of all convex combinations is called a _convex hull_.

**Definition (k-simplex)** A k-simplex is the convex hull of $k+1$ affinely
independent points. These point are the vertices of the simplex.
A $k$-simplex is a $k$-dimensional subspace of $\mathbb{R}^d$.
For a simplex $\sigma$, we denote this as $\text{dim}\sigma = k$.  
A zero dimensional simplex called a vertex, a one dimensional and edge, a two
dimensional a triangle.

**Definition (face, coface)** Let a $k$-simplex, $\sigma$ defined by set of 
vertices, $S = \{v_0,v_1,\ldots,v_k \}$. Then, a simplex $\tau$ defined by 
$T \subseteq S$ is a _face_ of $\sigma$. Every face of a simplex has a dual,
in this case, $\sigma$ is the dual, or _coface_ of $\tau$.  
This is denoted by $\tau \leq \sigma$.

**Definition (simplical complex)** A simplical complex $K$ is a finite set of
simplices with the following properties:

1. $\sigma \in K, \tau \leq \sigma \Rightarrow \tau \in K$,
2. $\sigma, \sigma' \in K \Rightarrow \sigma \cap \sigma' \leq \sigma, \sigma'$.

The dimension of the complex is $\text{dim}K=max\{\text{dim}\sigma|\sigma\in K\}$
The zero-simplices of K are the complex's vertices, and the
one-simplices its edges.

We can also define simplices without using geometry.

**Definition (abstract simplical complex)** An abstract simplical complex is
a set $K$ with a collection of subsets $s$ with the following properties:

1. $\forall v \in K, \{v\} \in \mathcal{S}$,
2. $\tau \subseteq \sigma \in \mathcal{S} \Rightarrow \tau \in \mathcal{S}$.

The sets $\{v\}$ are the vertices of K.

**Definition (vertex scheme)** Let $K$ be a simplical complex as above, with
$V$ as the set of its vertices, and let $\mathcal{K}$ be the collection of all
subsets $\{v_0, v_2, \ldots, \v_k\}$ of $V$ such that these vertices span $K$.
$\mathcal{K}$ is called the vertex scheme of $K$.

**Definition (underlying space)** $|K| = \cup_{\sigma \in K} \sigma$
is the underlying space of $K$.

$|K|$ is a topological space.

**Definition (triangulation)** A triangulation of a topological space
$\mathbb{X}$ is the simplical complex $K$ such that $|K| \approx \mathbb{X}$

**Definition (subcomplex)** If $L$ is a simplical complex such that $L
\subseteq K$, than we call $L$ the subcomplex of $K$.

**Definition (filtration)** The nested sequence of subcomplexes, $\emptyset
= K^0 \subseteq K^1 \subseteq K^2 \subseteq \ldots \subseteq K^m = K$ is called
the filtration of the complex $K$.  
$K$ is called a filtered complex.

**Definition (filtration ordering)** A filtration ordering of a simplical
complex $K$ is a full ordering of it simplices, in a way that each prefix of
the ordering is a subcomplex.

**Definition (homotopy)** Let $f_t : \mathbb{X} \rightarrow \mathbb{Y}, t \in
[0, 1]$ be a family of maps, such that the associated map $F : \mathbb{X}
\times [0, 1] \rightarrow \mathbb{Y}$ given by $F(x,t) = f_t(x)$ is continuous.
Then $f_t$ is a homotopy, and $f_0, f_1 : \mathbb{X} \rightarrow \mathbb{Y}$
are homotopic. We denote this with $f_0 \simeq f_1$.

**Definition (homotopy equivalence)** $\mathbb{X}$ and $\mathbb{Y}$ are
homotopy equivalent, and have the same homotopy type, if there is a map $f
: \mathbb{X} \rightarrow \mathbb{Y}$ and a map $g : \mathbb{Y} \rightarrow
\mathbb{X}$, such that $f \circ g \simeq 1$ and $g \circ f \simeq 1$.

We denote this as $\mathbb{X} \simeq \mathbb{Y}$.

**Theorem** $\mathbb{X} \approx \mathbb{Y} \Rightarrow \mathbb{X} \simeq \mathbb{Y}$ [@zomorodian2005topology].

The proof of this theorem is beyond the scope of this thesis, but to put it in
words: if two spaces are topological homomorphic, then that means that they are
also homotopically equivalent.

**Definition (open cover)** An open cover, $U$ of $S$ is

$$
U = \{ \mathbb{U}_i \}_{i \in I}, \, \mathbb{U}_i \subseteq \mathbb{Y},
$$

where $I$ is the index set, and $S \subseteq \cup_i \mathbb{U}_i$,
and $\mathbb{U}_i$ are open.

**Definition (nerve)** If

1. $\emptyset \in N$, and
2. $\cap_{j \in J} \mathbb{U}_j\not=\emptyset$ for $J\subseteq I\Rightarrow J \in N$, 

then N is a nerve of U. The nerve N is a simplical complex[@zomorodian2005topology].

High-dimensional homotopy groups ($\pi_n (X)$) are computationally very
expensive to compute, so in most cases they are not feasible to use. We can
exchange homotopy for homology, with some concessions, which uses combinatorial
structures for classification. To be exact, homology uses finitely generated
Abelian groups $H_n(X)$ to categorize the topology of the space, for every
integer $n \geq 0$ [@hatcher2002algebraic]. There are fast algorithms for
computing homology, but one drawback is that it is a weaker classifier then
homotopy [@zomorodian2005topology]. On top of this homology groups can directly
related to cell structures, and so they can be directly computed from them,
which the higher dimensional homotopy groups can not.
These groups have, among others, these two properties:

**Functoriality:** Each $H_n$ is a functor such that:

$$
\begin{tikzcd}[row sep=large]
  X \arrow[r, "f"] \arrow[d, "H_n" left]
    & Y \arrow[d, "H_n"] \\
  H_n(X) \arrow[r, "H_n(f)"]
&H_n(Y) \end{tikzcd}
$$

and the functor composition ($H_n(fg) = H_n(f) H_n(g)$) and identity properties
($H_n(i_X) = i_{H_n(X)}$).

**Homotopy invariance:** If $f,g : X \to Y$ are homotopic, then $H_n(f)=H_n(g)$.
If $f$ is a homotopy equivalence, then $H_n(f)$ is an isomorphism
[@carlsson2005persistence].

**Definition (Euler characteristic)** Let $K$ be a simplical complex and
$s_i=\text{card} \{ \sigma \in K | \text{dim} \sigma = i \}$. Then the Euler
characteristic $\chi(K)$ of K is

$$
\chi(K) = \sum_{i=0}^{\text{dim}K} (-1)^is_i = 
\sum_{\sigma \in K - \{\emptyset \}} (-1)^{\text{dim}\sigma}
$$

We can get the Euler characteristic's invariance from the invariance of
homology, through the theory of _singular homology_, and so we can also compute
the topology of point and objects.

