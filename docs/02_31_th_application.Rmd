# Applying the theory

Topological data analysis as a field is fairly new, but shares some methods to more established data analysis methods. One example is clustering.

## Density Clustering

Density clustering is one of the oldest and simplest topological data analysis method [wasserman2016topological].
